from setuptools import setup, find_namespace_packages

setup(
    name="NEMO_account_extras",
    version="1.1.2",
    packages=find_namespace_packages(),
    include_package_data=True,
    package_data={
        '': ['templates/customizations/*.html'],
    },
)