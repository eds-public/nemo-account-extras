from NEMO.models import BaseModel, Account
from NEMO.views.constants import CHAR_FIELD_MAXIMUM_LENGTH
from django.core.exceptions import ValidationError
from django.db import models

class FundingSourceType(BaseModel):
    name = models.CharField(
        max_length=CHAR_FIELD_MAXIMUM_LENGTH, unique=True, help_text="The internal funding source type."
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Funding source type"
        verbose_name_plural = "Funding source types"
        ordering = ["name"]

class AccountExtras(BaseModel):
    account = models.OneToOneField(Account, on_delete=models.CASCADE, related_name="extras", help_text="The account to which these details belong.")
    funding_source_type = models.ForeignKey(
        FundingSourceType,
        on_delete=models.SET_NULL,
        related_name="accounts",
        null=True,
        blank=True,
        help_text="The internal funding source type for this account.",
    )
    funding_source_identifier = models.CharField(
        max_length=CHAR_FIELD_MAXIMUM_LENGTH,
        null=True,
        blank=True,
        help_text="The internal funding source identifier for this account.",
    )

    def __str__(self):
        return f"{self.account} extras"

    def clean(self):
        if self.funding_source_identifier:
            not_unique = AccountExtras.objects.filter(funding_source_identifier=self.funding_source_identifier).exclude(
                account=self.account
            )
            if not_unique.exists():
                raise ValidationError(
                    f'The funding source identifier "{self.funding_source_identifier}" is already in use by another account.'
                )
            
    class Meta:
        verbose_name = "Account extras"
        verbose_name_plural = "Account extras"

def get_account_extras(account: Account):
    try:
        account_extras = account.extras
    except (AccountExtras.DoesNotExist, AttributeError, Account.extras.RelatedObjectDoesNotExist):
        account_extras = AccountExtras(account=account)
    return account_extras