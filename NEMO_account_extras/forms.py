from django.forms import HiddenInput, ModelForm
from django.core.exceptions import ValidationError
import re

from NEMO_account_extras.customizations import AccountExtrasCustomization
from NEMO_account_extras.models import AccountExtras


# Base details form for admin. Setting disabled and required attributes.
class AccountExtrasAdminForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        disable_fields, require_fields = AccountExtrasCustomization.disable_require_fields()
        for field_name in require_fields:
            if field_name in self.fields:
                self.fields[field_name].required = True
        for field_name in disable_fields:
            if field_name in self.fields:
                self.fields[field_name].disabled = True
                self.fields[field_name].required = False
                self.fields[field_name].widget = HiddenInput()

    def clean_funding_source_identifier(self):
        funding_source_identifier = self.cleaned_data.get('funding_source_identifier')
        if funding_source_identifier and not re.match(r'^\d{2}-\d{7}$', funding_source_identifier):
            raise ValidationError('Funding source identifier must match the format xx-xxxxxxx.')
        return funding_source_identifier

    class Meta:
        model = AccountExtras
        fields = "__all__"

# Details form for admin page.
class AccountExtrasForm(AccountExtrasAdminForm):
    def save(self, commit=True):
        details: AccountExtras = super().save(commit=commit)
        return details
