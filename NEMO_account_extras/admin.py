from NEMO.admin import AccountAdmin
from NEMO.models import Account
from django.contrib import admin

from NEMO_account_extras.forms import AccountExtrasAdminForm
from NEMO_account_extras.models import FundingSourceType, AccountExtras


class AccountExtrasInline(admin.StackedInline):
    form = AccountExtrasAdminForm
    model = AccountExtras
    can_delete = False
    min_num = 1


class NewAccountAdmin(AccountAdmin):
    inlines = [AccountExtrasInline] + list(AccountAdmin.inlines)


# Re-register AccountAdmin
admin.site.unregister(Account)
admin.site.register(Account, NewAccountAdmin)

admin.site.register(FundingSourceType)
