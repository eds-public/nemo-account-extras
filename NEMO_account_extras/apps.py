from django.apps import AppConfig


class NemoAccountExtrasConfig(AppConfig):
    name = "NEMO_account_extras"
    verbose_name = "NEMO Account Extras"

    def ready(self):
        """
        This code will be run when Django starts.
        """
        pass
