from typing import Set, Tuple

from NEMO.decorators import customization
from NEMO.views.customization import CustomizationBase


@customization(title="Account extras", key="account_extras")
class AccountExtrasCustomization(CustomizationBase):
    variables = {
        "account_extras_enable_funding_source_identifier": "",
        "account_extras_require_funding_source_identifier": "",
        "account_extras_enable_funding_source_type": "",
        "account_extras_require_funding_source_type": "",
    }

    @classmethod
    def disable_require_fields(cls) -> Tuple[Set[str], Set[str]]:
        disable_fields, require_fields = set(), set()
        for var in cls.variables:
            if var.startswith("account_extras_enable_"):
                enable_field_name = var.replace("account_extras_enable_", "")
                if not cls.get_bool(var):
                    disable_fields.add(enable_field_name)
            elif var.startswith("account_extras_require_"):
                require_field_name = var.replace("account_extras_require_", "")
                if cls.get_bool(var):
                    require_fields.add(require_field_name)
                    if require_field_name in disable_fields:
                        disable_fields.remove(require_field_name)
        return disable_fields, require_fields
