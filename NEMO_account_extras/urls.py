from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path("NEMO_account_extras", TemplateView.as_view(template_name="NEMO_account_extras/index.html")),
    # Add your urls here.
]
