# Generated by Django 3.2.25 on 2024-06-14 17:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    operations = [
        migrations.CreateModel(
            name="FundingSourceIdentifier",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                (
                    "name",
                    models.CharField(help_text="The internal funding source identifier.", max_length=255, unique=True),
                ),
            ],
            options={
                "verbose_name": "Funding source identifier",
                "verbose_name_plural": "Funding source identifiers",
                "ordering": ["name"],
            },
        ),
        migrations.CreateModel(
            name="AccountExtras",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                (
                    "account",
                    models.OneToOneField(
                        help_text="The account to which these details belong.",
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="extras",
                        to="NEMO.account",
                    ),
                ),
                (
                    "funding_source_identifier",
                    models.ForeignKey(
                        blank=True,
                        help_text="The internal funding source identifier for this account.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="accounts",
                        to="NEMO_account_extras.fundingsourceidentifier",
                    ),
                ),
            ],
            options={
                "verbose_name": "Account extras",
                "verbose_name_plural": "Account extras",
                "ordering": ["funding_source_identifier"],
            },
        ),
    ]
